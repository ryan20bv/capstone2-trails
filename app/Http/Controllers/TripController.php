<?php

namespace App\Http\Controllers;

use \App\Trip;
use \App\Category;
use \App\Status;
use \App\Tripinfo;
use Auth;
use Session;
use \App\User;


use Illuminate\Http\Request;

class TripController extends Controller
{
    // this is to see the trip catalog
    public function trip(){
        $trips = Trip::all();
        // $user = Auth::user()->id;
        return view('trips', compact('trips'));
    }

    // this is to view add trip page
    public function addTrip(){
        $categories = Category::all();
        $statuses = Status::all();
        return view('creatorviews.addtrip', compact('categories', 'statuses'));
    }

    //this is to save the trip
    public function store(Request $req){
        if(Auth::user()){
            $rules = array(
                "title" => "required",
                "destination" => "required",
                "dateinfo" => "required",
                "noOfParticipants" => "required | numeric",
                "price" => "required | numeric",
                "category_id" => "required",
                "imgPath" => "required | image | mimes:jpeg,png,jpg",
                "details" => "required"
            );

            $this -> validate($req, $rules);
            $newTrip = new Trip;
            $newTrip->title = $req->title;
            $newTrip->destination = $req->destination;
            $newTrip->dateinfo = $req->dateinfo;
            $newTrip->noOfParticipants = $req->noOfParticipants;
            $newTrip->price = $req->price;
            $newTrip->category_id = $req->category_id;
            $newTrip->details = $req->details;
            $newTrip->status_id=1;
            $newTrip->creator_id = Auth::user()->id;

            $image = $req->file('imgPath');
        	$image_name = time().".".$image->getClientOriginalExtension();
        	$destination = "images/"; //corresponds to the public images directory
            $image->move($destination, $image_name);
            $newTrip->imgPath = $destination.$image_name;

            $newTrip->save();
        	Session::flash("message", "$newTrip->title has been added");
        	return redirect('/trips');
        
        }else{
            return redirect('login');
        }

    }


    // this is to view edit trip page

    public function editTrip($id){
        $trip = Trip::find($id);
        $categories = Category::all();
        return view('creatorviews.edittrip', compact("trip", "categories"));
    }

    // this is to update trip 
    public function update($id, Request $req){
        $trip = Trip::find($id);
        $rules = array(
            "title" => "required",
            "destination" => "required",
            "dateinfo" => "required",
            "noOfParticipants" => "required | numeric",
            "price" => "required | numeric",
            "category_id" => "required",
            "imgPath" => "image | mimes:jpeg,png,jpg",
            "details" => "required",
        );

        $this -> validate($req, $rules);

        $trip->title = $req->title;
        $trip->destination = $req->destination;
        $trip->dateinfo = $req->dateinfo;
        $trip->noOfParticipants = $req->noOfParticipants;
        $trip->price = $req->price;
        $trip->details = $req->details;
        $trip->category_id = $req->category_id;
        $trip->status_id=1;

        if($req->file('imgPath') != null){
            $image = $req->file('imgPath');
            $image_name = time().".".$image->getClientOriginalExtension();

            $destination = "images/"; //corresponds to the public images directory
            $image->move($destination, $image_name);
            $trip->imgPath = $destination.$image_name;
        }

        $trip->save();
    	Session::flash("message", "$trip->title has been updated");
    	return redirect('/trips');
    }

    // this is to destroy/delete trip
    public function destroy($id){
        $tripToDelete = Trip::find($id);
        $tripToDelete->delete();

        return redirect('/trips');

    }

    // this is to view trip details page
    public function tripInfo($id){
        $trip = Trip::find($id);
        $categories = Category::all();
      
        return view('creatorviews.tripinfo', compact('trip','categories'));
    }


    // this is to join the trip
    public function joinTrip($id, Request $req){
        if(Session::has('join')){
            $join = Session::get('join');
        }else{
            $join = [];
        }

        Session::put('join', $join);
        $trip = Trip::find($id);
            Session::flash("message", "$trip->title has been added to trips");

        // dd($trip);

        return redirect()->back();

    }

    public function showTrip(){
        $trips = Trip::all();

        $trips =[];
        $total = 0;


        if(Session::has('join')){
            $join=Session::get('join');
            // cart is in associative array
            foreach($join as $tripId){
                $trip = Trip::find($tripId);
                // $item->quantity = $quantity;
                // $item->subtotal = $item->price * $quantity;
                $trips[] = $trip; //this is how to push
                $total += $trip->price;
            }
        }
        // dd($trips->id);

        return view('/userviews.tripsjoined', compact('trips'));
    }







}
