<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function category(){
        return $this -> belongsTo("\App\Category");
    }
    public function status(){
        return $this -> belongsTo("\App\Status");
    }

    public function user(){
        return $this -> belongsTo("\App\User");
    }

    // public function user(){
    // 	// return $this->belongsToMany("\App\Order");//if no pivot column only compose of the id
    // 	return $this->belongsToMany("\App\user")->withPivot("comment_id")->withTimeStamps();
    // }

     public function tripinfo(){
        return $this -> belongsTo("\App\Tripinfo");
    }
}
