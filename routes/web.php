<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
	//to view trip page
Route::get('/trips', 'TripController@trip');


// creator route
	// to view trip page
Route::get('/addtrip', 'TripController@addTrip');
	// to update trip
Route::post('/addtrip', 'TripController@store');
	// to view edit trip page
Route::get('/edittrip/{id}', 'TripController@editTrip');
	// to update trip
Route::Patch('/edittrip/{id}', 'TripController@update');
	// to delete trip
Route::delete('/deletetrip/{id}', 'TripController@destroy');
	//to see the details
Route::get('/tripinfo/{id}', 'TripController@tripInfo');
	// to update the details 
Route::patch('/updatedetail/{id}', 'TripController@updateDetail');



// end of creator route


// joiners route
	// joining trip
Route::post('/jointrip/{id}', 'TripController@joinTrip');

Route::get('/tripsjoined', 'TripController@showTrip');



// end joiners route


// common route


// end of common route
