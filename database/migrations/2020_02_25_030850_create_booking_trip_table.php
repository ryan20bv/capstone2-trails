<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_trip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('review_id');
            $table->unsignedBigInteger('booking_id');
            $table->unsignedBigInteger('trip_id');
            $table->timestamps();

            $table->foreign('review_id')
            ->references('id')
            ->on('reviews')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('booking_id')
            ->references('id')
            ->on('bookings')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('trip_id')
            ->references('id')
            ->on('trips')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_trip');
    }
}
