<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('statuses')->delete();
        \DB::table('statuses')->insert(array(
        	0=>
        	array(
        		'id'=> 1,
        		'name'=>'Open',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	1=>
        	array(
        		'id'=> 2,
        		'name'=>'Close',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	2=>
        	array(
        		'id'=> 3,
        		'name'=>'Departed',
        		'created_at'=> NULL,
        		'updated_at'=>NULL

            ),
            3=>
        	array(
        		'id'=> 4,
        		'name'=>'Arrived the destination',
        		'created_at'=> NULL,
        		'updated_at'=>NULL
        	),
        	4=>
        	array(
        		'id'=> 5,
        		'name'=>'Departed the destination',
        		'created_at'=> NULL,
        		'updated_at'=>NULL

            ),
            5=>
        	array(
        		'id'=> 6,
        		'name'=>'Returned Safely',
        		'created_at'=> NULL,
        		'updated_at'=>NULL

            )
            6=>
        	array(
        		'id'=> 7,
        		'name'=>'Pending',
        		'created_at'=> NULL,
        		'updated_at'=>NULL

            ),
            7=>
        	array(
        		'id'=> 8,
        		'name'=>'Paid',
        		'created_at'=> NULL,
        		'updated_at'=>NULL

        	)
        ));
    }
}
