@extends('layouts.app')
@section('content')
	<h1 class="text-center py-2">Trips Joined</h1>
	{{-- for validation on pay via COD --}}
	{{-- @if($trips != null) --}}

	{{-- end for validation on pay via COD --}}
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Trip Title:</th>
							<th>Trip Destination:</th>
							<th>Trip Date:</th>
							<th>Trip price:</th>
							<th></th>

						</tr>
					</thead>
					<tbody>
						@foreach($trips as $trip)
							<tr>
								<td>{{$trip->title}}</td>
								<td>{{$trip->destination}}</td>
								<td>{{$trip->dateinfo}}</td>
								<td>{{$trip->price}}</td>
								<td>
									{{-- fefb 24, 2020 --}}
									<form action="/removeitem/{{$item->id}}" method="POST" 	>
										@csrf
										@method('delete')
										<button class="btn btn-danger" type="submit">Remove</button>
										
									</form>
								</td>
							</tr>
						@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
						{{-- 	<td>Total: {{$total}}</td> --}}
							<td>
								{{-- <form action="/emptycart" method="POST">
										@csrf
										@method('delete')
										<button class="btn btn-danger" type="submit">Empty Cart</button>
										
								</form> --}}
								<a href="/emptycart" class="btn btn-danger">Empty</a>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<a href="/checkout" class="btn btn-secondary">Pay Via COD</a>
							</td>
							<td></td>
							<td></td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	{{-- @else
		<h2 class="text-center py-5">Join a Trip!!!</h2>
	@endif --}}
@endsection