@extends('layouts.app')
@section('content')
	<div class="container">
		<h1 class="text-center py-1">All Trips</h1>

		@if(Session::has("message"))
			<h4>{{Session::get('message')}}</h4>
		@endif
		<div class="row">
			@foreach($trips as $trip)
				<div class="col-lg-4 my-1">
					<div class="card">
						<img src="{{asset($trip->imgPath)}}" class="card-img-top border" alt="Nothing" height="250px">
						<div class="card-body">
							<h4 class="card-title">{{$trip->title}}</h4>
                            <p class="card-text">{{ $trip->destination }}</p>
                            <p class="card-text">{{ $trip->dateinfo }}	</p>
                            <p class="card-text">No of Persons: {{ $trip->noOfParticipants }}	</p>
                            <p class="card-text">Price: Php {{ $trip->price }}	per head</p>
                            <p class="card-text">{{ $trip->category->name }}	</p>
                            <p class="card-text">{{ $trip->status->name}}	</p>
                            <p class="card-text">created by: {{$trip->creator_id}}	</p>
                            {{-- @dd($trip->creator->name) --}}
                        </div>
                   
                    <a href="/tripinfo/{{$trip->id}}" class="btn btn-info">more details</a>

                    <form action="/jointrip/{{$trip->id}}" method="POST">
                    	@csrf
                    	<button class="btn btn-secondary btn-block">Join</button>
                    </form>
                    </div>



					<div class="card-footer d-flex">
						<form method="POST" action="/deletetrip/{{$trip->id}}">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger ">Delete</button>
						</form>
						<a href="/edittrip/{{$trip->id}}" class="btn btn-info">Edit</a>

					</div>
					{{-- <div class="card-footer">
						<form action="/addtocart/{{$indiv_item->id}}" method="POST">
							@csrf
							<input type="number" name="quantity" class="form-control" value="1">
							<button class="btn btn-secondary btn-block" type="submit">Add to cart</button>
						</form>
					</div> --}}
				</div>
			@endforeach
		</div>

	</div>


@endsection
