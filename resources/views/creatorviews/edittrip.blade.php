{{-- @extends('layouts.app')
@section('content')
	<h1 class="text-center py-1">EDIT TRIP</h1>
	<div class="col-lg-6 offset-lg-3">
    <form method="POST" action="/edittrip/{{$trip->id}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
			<div class="form-group">
				<label for="title">Title:</label>
            <input type="text" name="title" class="form-control" value="{{$trip->title}}">
			</div>
			<div class="form-group">
				<label for="destination">Destination:</label>
				<input type="text" name="destination" class="form-control" value="{{$trip->destination}}">
			</div>
			<div class="form-group">
				<label for="dateinfo">Date Info:</label>
				<input type="text" name="dateinfo" class="form-control"  value="{{$trip->dateinfo}}">
            </div>
            <div class="form-group">
				<label for="noOfParticipants">No. of participants:</label>
				<input type="number" name="noOfParticipants" class="form-control" value="{{$trip->noOfParticipants}}">
            </div>
            <div class="form-group">
				<label for="price">Price/head:</label>
				<input type="number" name="price" class="form-control" value="{{$trip->price}}">
            </div>
            <img src="{{$trip->imgPath}}" class="card-img-top border" alt="Nothing" height="50px">
			<div class="form-group">
				<label for="imgPath">Image:</label>
				<input type="file" name="imgPath" class="form-control">
			</div>
			<div class="form-group">
				<label for="category_id">Category: </label>
				<select name="category_id" class="form-control">
					@foreach($categories as $category)
						<option value="{{$category->id}}" {{$trip->category_id == $category->id ? "selected" : " "}}>{{$category->name}}</option>
					@endforeach
				</select>
            </div>
            <button class="btn btn-primary" type="submit">Update Trip</button>
		</form>

	</div>
@endsection --}}


@extends('layouts.app')
@section('content')
	<h1 class="text-center py-1">EDIT TRIP</h1>

		<form method="POST" action="/edittrip/{{$trip->id}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
			<div class="row">
				<div class="col-lg-3 offset-lg-1">	
					<div class="form-group">
						<label for="title">Title:</label>
						<input type="text" name="title" class="form-control" value="{{$trip->title}}">
					</div>
					<div class="form-group">
						<label for="destination">Destination:</label>
						<input type="text" name="destination" class="form-control" value="{{$trip->destination}}">
					</div>
					<div class="form-group">
						<label for="dateinfo">Date Info:</label>
						<input type="text" name="dateinfo" class="form-control" value="{{$trip->dateinfo}}">
		            </div>
		            <div class="form-group">
						<label for="noOfParticipants">No. of participants:</label>
						<input type="number" name="noOfParticipants" class="form-control" value="{{$trip->noOfParticipants}}">
		            </div>
		            <div class="form-group">
						<label for="price">Price/head:</label>
						<input type="number" name="price" class="form-control" value="{{$trip->price}}">
					</div>
					<div class="form-group">
						<label for="imgPath">Image:</label>
						<input type="file" name="imgPath" class="form-control">
					</div>
					<div class="form-group">
						<label for="category_id">Category: </label>
						<select name="category_id" class="form-control">
							@foreach($categories as $category)
								<option value="{{$category->id}}" {{$trip->category_id == $category->id ? "selected" : " "}}>{{$category->name}}</option>
							@endforeach
						</select>
		            </div>
				</div>
				<div class="col-lg-6 offset-lg-1">
					<h3 class="text-center">Details:</h3>
					<div class="form-group">
						
						<textarea name="details" id="" cols="50" rows="20">{{$trip->details}}</textarea>
					</div>
					
				</div>
			</div>
			
				<button class="btn btn-primary" type="submit">Edit Trip</button>
          	
		</form>
	
	
@endsection

