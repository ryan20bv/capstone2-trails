@extends('layouts.app')
@section('content')
	<h1 class="text-center py-1">ADD TRIP</h1>

		<form method="POST" action="/addtrip" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col-lg-3 offset-lg-1">	
					<div class="form-group">
						<label for="title">Title:</label>
						<input type="text" name="title" class="form-control">
					</div>
					<div class="form-group">
						<label for="destination">Destination:</label>
						<input type="text" name="destination" class="form-control">
					</div>
					<div class="form-group">
						<label for="dateinfo">Date Info:</label>
						<input type="text" name="dateinfo" class="form-control">
		            </div>
		            <div class="form-group">
						<label for="noOfParticipants">No. of participants:</label>
						<input type="number" name="noOfParticipants" class="form-control">
		            </div>
		            <div class="form-group">
						<label for="price">Price/head:</label>
						<input type="number" name="price" class="form-control">
					</div>
					<div class="form-group">
						<label for="imgPath">Image:</label>
						<input type="file" name="imgPath" class="form-control">
					</div>
					<div class="form-group">
						<label for="category_id">Category: </label>
						<select name="category_id" class="form-control">
							@foreach($categories as $indiv_category)
								<option value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
							@endforeach
						</select>
		            </div>
				</div>
				<div class="col-lg-6 offset-lg-1">
					<h3 class="text-center">Details:</h3>
					<div class="form-group">
						
						<textarea name="details" id="" cols="50" rows="20"></textarea>
					</div>
					
				</div>
			</div>
			
				<button class="btn btn-primary" type="submit">Add Trip</button>
          	
		</form>
	
	
@endsection
