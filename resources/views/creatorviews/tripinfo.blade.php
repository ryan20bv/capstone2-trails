@extends('layouts.app')
@section('content')
    <h1 class="text-center py-1">TRIP DETAILS</h1>
   {{--  <div class="container"> --}}
        <div class="row w-100">
            <div class="col-lg-3 border">
                <img src="{{asset($trip->imgPath)}}" class="card-img-top border" alt="Nothing" height="100px" width="30px">
                    <div class="card-body">
                        <h4 class="card-title">Title: {{$trip->title}}</h4>
                        <p class="card-text">Destination: {{ $trip->destination }}</p>
                        <p class="card-text">Date: {{ $trip->dateinfo }}	</p>
                        <p class="card-text">No of Persons: {{ $trip->noOfParticipants }}	</p>
                        <p class="card-text">Price: Php {{ $trip->price }}	per head</p>
                        <p class="card-text">Category: {{ $trip->category->name }}	</p>
                        <p class="card-text">Status: {{ $trip->status->name}}	</p>
                        <p class="card-text">Created by: {{ $trip->creator_id}}   </p>
                    </div>
             </div>
             <div class="col-lg-6 border px-2">
                <h1 class="text-center py-3">Details</h1>
     
                 <p>
                    {{$trip->details}}
                 </p>
              
             </div>
             <div class="col-lg-1 border">
                 <h3 class="text-center">List of Participants</h3>
             </div>
             
        </div>


   {{--  </div> --}}



@endsection
